#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <time.h>
#include <string>
#include <Windows.h>


using namespace sf;

void finish(RenderWindow &window)
{
	
	Texture writing;
	writing.loadFromFile("napis.png");

	Sprite sprite;
	sprite.setTexture(writing);
	sprite.setOrigin(150, 31);
	sprite.setPosition(960, 100);

	window.draw(sprite);
}

class Cosmic
{
	public:
	const Vector2f borders = {1920, 1080};
	Vector2f speedVector;
	Sprite sprite;

	float speedValue()		//liczy wartosc predkosci z wektora
	{
		return sqrt(pow(this->speedVector.x, 2) + pow(this->speedVector.y, 2));
	}

	float degsToRads(float angleDeg)	// Przelicza stopnie na radiany
	{
		return angleDeg / 57.2957795;
	}

	float modifierX()		//Zwraca sinus kata, pod jakim ustawiony jest obiekt
	{
		float temp = degsToRads(this->sprite.getRotation());
		return sin(temp);					
	}

	float modifierY()		//zwraca -cos kata, pod jakim ustawiony jest obiekt
	{
		float temp = degsToRads(this->sprite.getRotation());
		return -cos(temp);
	}

	bool collidesWith(Cosmic cos)		//Wykrywa kolizje obiektu, ktory wywoluje metode z obiektem podanym jako argument metody
	{
		Vector2f firstLocus = this->sprite.getPosition();
		Vector2f secondLocus = cos.sprite.getPosition();
		float distance = sqrt(pow((firstLocus.x - secondLocus.x), 2) + pow((firstLocus.y - secondLocus.y), 2));
		if(distance < 60)
		{
			return true;
		}

		return false;
	}
	
};


class Car : public Cosmic		//Car posiada predkosc, zdolnosc do obracania, punkty zycia. Ma ograniczona maksymalna predkosc.
{	
	public:
	
	const double rotator = 0.23;
	const double speedLimit = 0.9;
	int hp;


	Car()
	{
		this->sprite.setOrigin(40, 45);		// Origin to os obrotu. Ustawiam ja na srodku obiektu.
		this->speedVector.x = 0;
		this->speedVector.y = 0;
		this->hp = 1000;
		this->hp = 1000;
	}

	Car(Sprite sprite, Texture texture)		//zerowy kat, wysrodkowany pivot, 
	{
		this->sprite = sprite;
		this->sprite.setTexture(texture);
		this->sprite.setOrigin(40, 45);
		this->speedVector.x = 0;
		this->speedVector.y = 0;
		this->hp = 1000;
	}

	void turnRight()
	{
		this->sprite.rotate(rotator);
	}

	void turnLeft()
	{
		this->sprite.rotate(-rotator);
	}

	/*
	void alternativeRight()
	{
		this->sprite.rotate(rotator);
		float temp = this->speedValue();
		this->speedVector.x = cos(degsToRads(this->sprite.getRotation())) * temp;
		this->speedVector.y = sin(degsToRads(this->sprite.getRotation())) * temp;
	}

	void alternativeLeft()
	{
		this->sprite.rotate(-rotator);
		float temp = this->speedValue();
		this->speedVector.x = cos(degsToRads(this->sprite.getRotation())) * temp;
		this->speedVector.y = sin(degsToRads(this->sprite.getRotation())) * temp;
	}
*/
	void accelerate()
	{
		float temp = degsToRads(this->sprite.getRotation());
		float modifierX = sin(temp);								//modifier to sinus kata
		temp = degsToRads(this->sprite.getRotation());
		float modifierY = -cos(temp);

		if(this->speedValue() <= speedLimit)		
		{
			this->speedVector.x += (0.0012 * modifierX);
			this->speedVector.y += (0.0012 * modifierY);
		}
		/*else
		{
			if(modifierX >= 0 && modifierY <= 0)
			{
				this->speedVector.y -= this->speedValue() - (0.0001 * modifierY);
				this->speedVector.x += this->speedValue() - (0.0001 * modifierX);
			}
		}*/
	}

	void brake()
	{
		this->speedVector.x /= 1.004; 
		this->speedVector.y /= 1.004;
	}

	void idle()		//Pojazd wytraca predkosc, jesli nie jest ona dodawana. Opory powietrza w prozni!
	{
		this->sprite.move(this->speedVector.x, this->speedVector.y);
		this->speedVector.x /= (100/99.94);
		this->speedVector.y /= (100/99.94);
	}

	void handleTight()			//Funkcja pilnuje, zeby obiekt nie zblakal sie poza obszar ekranu.
	{
		Vector2f temp = this->sprite.getPosition();
		if(temp.x < 0)
		{
			this->sprite.move(1, 0);
			this->speedVector.x = 0;
		}

		if(temp.x > this->borders.x)
		{
			this->sprite.move(-1, 0);
			this->speedVector.x = 0;
		}

		if(temp.y < 0)
		{
			this->sprite.move(0, 1);
			this->speedVector.y = 0;
		}

		if(temp.y > this->borders.y)
		{
			this->sprite.move(0, -1);
			this->speedVector.y = 0;
		}
	}

	void jump()		//Skok, ktory jest mozliwy do wykonania raz na trzy sekundy.
	{
		this->sprite.move(this->speedVector.x * 200, this->speedVector.y * 200);
	}
};


class Bullet : public Cosmic		
{
	public:
	const double speedLimit = 1.5;

	Texture texture;
	Clock clock;
	Time timer;
	bool readyToUse;
	bool exploding;

	Bullet(Car player1)
	{
		this->texture.loadFromFile("bullet.png");
		this->sprite.setTexture(this->texture);
		this->sprite.setOrigin(10, 22);
		this->sprite.setPosition(player1.sprite.getPosition());
		this->sprite.setRotation(player1.sprite.getRotation());
		this->speedVector = player1.speedVector;
		this->readyToUse = true;
		this->exploding = false;
	}

	Bullet()
	{
		this->readyToUse = true;
		this->sprite.setPosition(-50, -50);
	}

	~Bullet()
	{
	}


	void live()
	{
		if(this->outOfBounds() == false)
		{
			this->sprite.move(this->speedVector.x, this->speedVector.y);
		}
		else
		{
			this->reset();
		}
	}

	bool outOfBounds()
	{
		Vector2f temp = this->sprite.getPosition();
		if(temp.x < 0 || temp.x > this->borders.x || temp.y < 0 || temp.y > this->borders.y)
		{
			return true;
		}

		return false;
	}

	void reset()
	{
		this->speedVector.x = this->speedVector.y = 0;
		this->sprite.setPosition(-50, -50);
		this->readyToUse = true;
		this->exploding = false;
	}
};

class BulletManager
{
	public:
	const static int amount = 100;		//Okresla maksymalna ilosc pociskow wystrzelonych przez jednego gracza
	Bullet bullets [amount];
	RenderWindow* window;		
	Sprite spriteBase;
	Car* attacker;
	
	
	short controller;

	BulletManager()
	{
		this->controller = 0;
	}

	void shoot()
	{
		int i = 0;
		while(this->bullets[i].readyToUse != true && i < this->amount)
		{
			i++;
		}

		this->bullets[i].readyToUse = false;

		this->bullets[i].speedVector.x = this->attacker->modifierX() * this->bullets[i].speedLimit;
		this->bullets[i].speedVector.y = this->attacker->modifierY() * this->bullets[i].speedLimit;

		this->bullets[i].sprite.setRotation(attacker->sprite.getRotation());
		this->bullets[i].sprite.setPosition(attacker->sprite.getPosition());
	}

	void goAround()						//To funkcja za ktora kryje sie ruch wszystkich wystrzelonych pociskow, ktore nie opuscily planszy.
	{
		for(int i = 0; i < this->amount; i++)
		{
			if(this->bullets[i].readyToUse == false)
			{
				this->bullets[i].live();
			}
		}
	}

	void show()
	{
		for(int i = 0; i < this->amount; i++)
		{
			if(this->bullets[i].readyToUse == false)
			{
				this->window->draw(bullets[i].sprite);
			}

		}
	}

};

class Warp : public Cosmic	//Warp wyrzuca gracza w losowe miejsce planszy.
{
	public:
	void teleport(Cosmic &object)
	{
		int a, b;
		a = std::rand() % 1920;			// O wlasnie tak - losujac wspolrzedne.
		b = std::rand() % 1080;
		object.sprite.setPosition(a, b);
	}

	Warp()
	{
		this->sprite.setOrigin(55, 55);
	}
};

class HealthBar
{
	public:
	Car* player;
	bool tiles [5];		//Rzeczone kwadraty reprezentuja "tabele", ktora bedzie zapelniana lub pusta.
	Texture texture;
	Sprite sprite;

	HealthBar()
	{
		for(int i = 0; i < 5; i++)		//Na poczatku pasek zycia jest pelen.
		{
			this->tiles[i] = true;
		}
	}

	void update()
	{
		for(int i = 0; i < 5; i++)			//Pozniej porywnywana jest ilosc punktow zycia gracza z punktami orientacyjnymi (dwusetkami zycia).
		{									//Gracz moze miec 1000 punktow zycia. Kazdy kwadracik oznacza <k+1; k+200> zycia dla k = 0, 1, 2, 3, 4.
			if(this->player->hp > i * 200)
			{
				this->tiles[i] = true;
			}
			else
			{
				this->tiles[i] = false;
			}
		}
	}
	

};

class Meteor : public Cosmic
{
	public:
	Meteor()
	{
		this->sprite.setOrigin(75, 70);
	}

	void randomStart()
	{
		int x, y, sign;
		float speed;

		y = rand() % 1080;
		sign = rand() % 2;

		speed = ((float(rand() % 5 + 1) / 5));		//Losuje predkosc meteoru oraz kierunek, z jakiego nadleci

		if(sign == 0)
		{
			x = 2000;
			this->speedVector.x = -speed;
		}
		else
		{
			x = -180;
			this->speedVector.x = speed;
		}

		this->sprite.setPosition(x, y);
	}

	void roll()
	{
		this->sprite.rotate(-(this->speedVector.x / 10));		//Predkosc katowa jest proporcjonalna
		this->sprite.move(this->speedVector.x, 0);				//Do predkosci liniowej.
	}

	void struck(Car &object)
	{
		object.speedVector.x = this->speedVector.x;
		object.speedVector.y = 0.8;
	}
};

int main()		
{

	std::srand(time(0));

	// ********************************* W�ASNO�CI OKNA  I ZDARZE� *************************
	RenderWindow window(VideoMode(1920, 1080, 32), "Bardzo grywalne okno.", Style::Fullscreen);

	Event ev;

	// ********************************* DEKLARACJA TEKSTUR *********************

	Texture texturePlayer1;	
	Texture texturePlayer2;
	Texture textureBullet;
	Texture textureWarp;
	Texture stun;
	Texture background;
	Texture textureHealth1;
	Texture textureHealth2;
	Texture textureMeteor;

	// ********************************* NADANIE TEKSTUROM "KONKRET�W" *********************

	textureHealth1.loadFromFile("tileYellow.png");
	textureHealth2.loadFromFile("tileGreen.png");
	background.loadFromFile("kosmos.jpg");
	stun.loadFromFile("stun.png");
	textureWarp.loadFromFile("warp.png");
	textureBullet.loadFromFile("bullet.png");
	texturePlayer1.loadFromFile("ship1.png");
	texturePlayer2.loadFromFile("ship2.png");
	textureMeteor.loadFromFile("meteor.png");

	// ********************************* NADANIE CECH GRACZOM I OBIEKTOM **************

	Sprite spritePlayer1;
	spritePlayer1.setTexture(texturePlayer1);

	Sprite spritePlayer2;
	spritePlayer2.setTexture(texturePlayer2);

	Sprite back(background);

	Car player1;
	player1.sprite.setTexture(texturePlayer1);
	player1.sprite.setPosition(rand() % 1900, rand() % 1080);

	Car player2;
	player2.sprite.setTexture(texturePlayer2);
	player2.sprite.setPosition(rand() % 1900, rand() % 1080);

	Meteor meteor[4];

	for(int i = 0; i < 3; i++)
	{
		meteor[i].sprite.setTexture(textureMeteor);
		meteor[i].randomStart();
	}
	

	HealthBar healthBar1;
	HealthBar healthBar2;
	healthBar1.sprite.setTexture(textureHealth1);
	healthBar2.sprite.setTexture(textureHealth2);

	healthBar1.sprite.setPosition(80, 1000);
	healthBar2.sprite.setPosition(1820, 1000);

	healthBar1.player = &player1;
	healthBar2.player = &player2;

	BulletManager managerPlayerOne;
	managerPlayerOne.attacker = &player1;
	managerPlayerOne.window = &window;
	managerPlayerOne.spriteBase.setTexture(textureBullet);
	for(int i = 0; i < 50; i++)
	{
		managerPlayerOne.bullets[i].sprite.setTexture(textureBullet);
		managerPlayerOne.bullets[i].sprite.setOrigin(10, 22);
	}

	BulletManager managerPlayerTwo = managerPlayerOne;
	managerPlayerTwo.attacker = &player2;

	Warp warp1;
	warp1.sprite.setTexture(textureWarp);
	warp1.sprite.setPosition(rand() % 1900, rand() % 1080);

	Warp warp2 = warp1;
	warp2.sprite.setPosition(rand() % 1900, rand() % 1080);

	Warp warp3 = warp1;
	warp3.sprite.setPosition(rand() % 1900, rand() % 1080);


	//************************* TIMERY *************
	Clock clock1;
	Time timeBullet1 = clock1.getElapsedTime();

	Clock clock2;
	Time timeBullet2 = clock2.getElapsedTime();

	Clock clock3;
	Time timeJump1 = clock3.getElapsedTime();

	Clock clock4;
	Time timeJump2 = clock4.getElapsedTime();

	Clock clockMeteor;
	Time timeMeteor = clockMeteor.getElapsedTime();


	Clock clockMain;



	//************************ NAPISY *****************


	while(window.isOpen())
	{

		while(window.pollEvent(ev))
		{	
			if(ev.type == Event::Closed)
			{
				window.close();
			}
		}

		//************************ GRACZ 1 *******************************

		if(Keyboard::isKeyPressed(Keyboard::Left))
		{
			player1.turnLeft();
		}

		if(Keyboard::isKeyPressed(Keyboard::Right))
		{
			player1.turnRight();
		}

		if(Keyboard::isKeyPressed(Keyboard::Up))
		{
			player1.accelerate();
		}

		if(Keyboard::isKeyPressed(Keyboard::Down))
		{
			player1.brake();
		}

		if(Keyboard::isKeyPressed(Keyboard::Numpad1))		
		{
			timeBullet1 = clock1.getElapsedTime();
			if(timeBullet1.asMilliseconds() > 300)
			{
				managerPlayerOne.shoot();
				clock1.restart();
			}
		}

		if(Keyboard::isKeyPressed(Keyboard::Numpad2))
		{
			timeJump1 = clock3.getElapsedTime();
			if(timeJump1.asMilliseconds() > 3000)
			{
				player1.jump();
				clock3.restart();
			}
			
		}

		//************************ GRACZ 2 *******************************
		
		if(Keyboard::isKeyPressed(Keyboard::A))
		{
			player2.turnLeft();
		}

		if(Keyboard::isKeyPressed(Keyboard::D))
		{
			player2.turnRight();
		}

		if(Keyboard::isKeyPressed(Keyboard::W))
		{
			player2.accelerate();
		}

		if(Keyboard::isKeyPressed(Keyboard::S))
		{
			player2.brake();
		}

		if(Keyboard::isKeyPressed(Keyboard::T))
		{
			timeJump2 = clock4.getElapsedTime();
			if(timeJump2.asMilliseconds() > 3000)
			{
				player2.jump();
				clock4.restart();
			}
			
		}

		if(Keyboard::isKeyPressed(Keyboard::R))		
		{
			timeBullet2 = clock2.getElapsedTime();
			if(timeBullet2.asMilliseconds() > 300)
			{
				managerPlayerTwo.shoot();
				clock2.restart();
			}

		}

		// ********************** BEZCZYNNO�C I UTRZYMANIE W GRANICACH ******************

		player1.idle();					//Przemieszczanie, wyhamowywanie bez "gazu" i utrzymywanie w polu.
		player1.handleTight();
		player2.idle();
		player2.handleTight();

		healthBar1.update();
		healthBar2.update();

		for(int i = 0; i < 3; i++)
		{
			meteor[i].roll();
		}
			

		// *************************** RYSOWANIE ******************************************
		window.clear(Color::White);
		
		window.draw(back);
		window.draw(warp1.sprite);
		window.draw(warp2.sprite);
		window.draw(warp3.sprite);
		window.draw(player1.sprite);//nazwy
		window.draw(player2.sprite);
		for(int i = 0; i < 3; i++)
		{
			window.draw(meteor[i].sprite);
		}

		managerPlayerOne.goAround();		//Obs�uga bullet�w: przemieszczanie i wy�wietlanie.
		managerPlayerOne.show();
		managerPlayerTwo.goAround();
		managerPlayerTwo.show();


		for(int i = 0; i < 5; i++)
		{
			HealthBar temp1 = healthBar1;
			HealthBar temp2 = healthBar2;

			if(healthBar1.tiles[i] == true)
			{
				temp1.sprite.move((64 * i), 0);
				window.draw(temp1.sprite);
			}

			if(healthBar2.tiles[i] == true)
			{
				temp2.sprite.move((-64 * i), 0);
				window.draw(temp2.sprite);
			}
		}


		// ********************* KOLIZJE ******************************
		for(int i = 0; i < managerPlayerOne.amount; i++)
		{
			if(player1.collidesWith(managerPlayerTwo.bullets[i]))
			{
				player1.hp -= 30;
				managerPlayerTwo.bullets[i].reset();
			}

			if(player2.collidesWith(managerPlayerOne.bullets[i]))
			{
				player2.hp -= 30;
				managerPlayerOne.bullets[i].reset();
			}
		}

		if(player1.collidesWith(warp1))
		{
			warp1.teleport(player1);
		}

		if(player2.collidesWith(warp1))
		{
			warp1.teleport(player2);
		}

		if(player1.collidesWith(warp2))
		{
			warp2.teleport(player1);
		}

		if(player2.collidesWith(warp2))
		{
			warp2.teleport(player2);
		}

		if(player1.collidesWith(warp3))
		{
			warp3.teleport(player1);
		}

		if(player2.collidesWith(warp3))
		{
			warp3.teleport(player2);
		}

		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				if(meteor[i].collidesWith(meteor[j]) && i != j)
				{
					meteor[i].speedVector.x * (-1);
					meteor[j].speedVector.x * (-1);
				}
			}
		}

		/*		//Uzna�em, �e wprowadza to zbyt du�o chaosu.
		if(meteor.collidesWith(warp1))
		{
			warp1.teleport(meteor);
		}

		if(meteor.collidesWith(warp2))
		{
			warp1.teleport(meteor);
		}

		if(meteor.collidesWith(warp3))
		{
			warp1.teleport(meteor);
		}
		*/

		for(int i = 0; i < 3; i++)
		{
			if(player1.collidesWith(meteor[i]))
			{
				meteor[i].struck(player1);
			}
		}

		for(int i = 0; i < 3; i++)
		{
			if(player2.collidesWith(meteor[i]))
			{
				meteor[i].struck(player2);
			}
		}

		for(int i = 0; i < 3; i++)
		{
			if(meteor[i].sprite.getPosition().x < -200 || meteor[i].sprite.getPosition().x > 2000 || meteor[i].sprite.getPosition().y < -100 || 
			meteor[i].sprite.getPosition().y > 1150)
			{
				meteor[i].randomStart();
			}
		}

	
		if(player1.hp < 0)
		{
			player1.speedVector.x = player1.speedVector.y = 0;
			player1.sprite.setTexture(stun);

			Texture writing;
			writing.loadFromFile("napis.png");

			finish(window);

		}

		if(player2.hp < 0)
		{
			player2.speedVector.x = player2.speedVector.y = 0;
			player2.sprite.setTexture(stun);

			window.draw(player2.sprite);

			finish(window);
		}

		/*
		for(int i = 0; i < managerPlayerOne.amount; i++)		//Pr�ba wywo�ywania pseudo-animacji rob�ysku pocisku :)
		{
			bool create = false;

			if(managerPlayerOne.bullets[i].collidesWith(player2))
			{
dw				managerPlayerOne.bullets[i].speedVector.x = managerPlayerOne.bullets[i].speedVector.y = 0;
				managerPlayerOne.bullets[i].timer = managerPlayerOne.bullets[i].clock.getElapsedTime();
				managerPlayerOne.bullets[i].exploding = true;
			}

			if(managerPlayerOne.bullets[i].exploding == true)
			{
				if((managerPlayerOne.bullets[i].clock.getElapsedTime().asMilliseconds() - managerPlayerOne.bullets[i].timer.asMilliseconds()) > 200)
				{
					managerPlayerOne.bullets[i].reset();
				}
			}
		}*/ 


		window.display();	
	}
}