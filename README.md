Welcome to the repository of my small interstellar game "Canis Playoris"!

The game is the very first "after hours" project I finished as a freshman.

Feel free to share your thoughts: michal.werda1@gmail.com.

![](images/Canis.jpg)